#cara mendefinisikan list
sebuah_list = ['Zorin OS',
               'Ubuntu',
               'FreeBSD',
               'NetBSD',
               'OpenBSD',
               'BackTrack',
               'Fedora',
               'Slackware']

#cara mendefinisikan tuple
sebuah_tuple = (0,1,2,3,4,5,6,7,8,9)

#cara mendefinisikan dictionary
sebuah_dictionary = {'nama':'Wiro Sableng',
                     'prodi':'Ilmu Komputer',
                     'email':'wirosableng@localhost',
                     'website':'http://www.sitampanggarang.com'}

#mengakses elemennya
print("mengakses salah satu elemen : ")
print("------------------------")
print(sebuah_list[5])
print(sebuah_tuple[8])
print(sebuah_dictionary['website'])

print("\n\n")

#mengakses beberapa elemen
print("mengakses beberapa elemen : ")
print("----------------------")
print(sebuah_list[2:5])
print(sebuah_tuple[3:6])

print("\n\n")

#mengakses elemennya dengan looping
print("mengakses semua elemen dengan looping for : ")
print("-------------------")

for sebuah in sebuah_list:
    print(sebuah),
print("\n")

for sebuah in sebuah_tuple:
    print(sebuah)
print("\n")

for sebuah in sebuah_dictionary:
    print(sebuah, ';', sebuah_dictionary[sebuah])