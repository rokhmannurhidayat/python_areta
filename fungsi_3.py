def cetak_biodata(nama="miki", kota="tangerang", umur=18):
    print("Nama : ",nama)
    print("Umur : ",umur)
    print("Kota : ",kota)
    return;

#kalau parameter diisi semua
print("tanpa memakai default argument : ")
cetak_biodata(umur=50, kota="bandung")

print("\n")

#kalau paramater tidak diisi semua
print("memakai default argument : ")
cetak_biodata()