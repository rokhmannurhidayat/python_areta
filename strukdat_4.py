#cara mendefinisikan list
sebuah_list = ['Zorin OS',
               'Ubuntu',
               'FreeBSD',
               'NetBSD',
               'OpenBSD',
               'BackTrack',
               'Fedora',
               'Slackware']

#cara mendefinisikan tuple
sebuah_tuple = (0,1,2,3,4,5,6,7,8,9)

#cara mendefinisikan dictionary
sebuah_dictionary = {'nama':'Wiro Sableng',
                     'prodi':'Ilmu Komputer',
                     'email':'wirosableng@localhost',
                     'website':'http://www.sitampanggarang.com'}

#cara menambahkan data baru
print("cara menambahkan data baru : ")
print("\n")

print(sebuah_list)
list_baru = sebuah_list + ['PC Linux OS', 'Blankon', 'IGOS', 'OpenSUSE']
print(list_baru)
print("\n")

print(sebuah_tuple)
tuple_baru = sebuah_tuple + (100, 200, 300)
print(tuple_baru)
print("\n")

print(sebuah_dictionary)
dictionary_baru = {'telp':'022-12345678','alamat':'Bandung, Jabar'}
sebuah_dictionary.update(dictionary_baru)
print(sebuah_dictionary)
print("\n")