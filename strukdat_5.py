#cara mendefinisikan list
sebuah_list = ['Zorin OS',
               'Ubuntu',
               'FreeBSD',
               'NetBSD',
               'OpenBSD',
               'BackTrack',
               'Fedora',
               'Slackware']

#cara mendefinisikan tuple
sebuah_tuple = (0,1,2,3,4,5,6,7,8,9)

#cara mendefinisikan dictionary
sebuah_dictionary = {'nama':'Wiro Sableng',
                     'prodi':'Ilmu Komputer',
                     'email':'wirosableng@localhost',
                     'website':'http://www.sitampanggarang.com'}

#cara delete sebuah elemen
print("cara delete sebuah elemen : ")
print("\n")

print(sebuah_list)
del sebuah_list[0]
print(sebuah_list)
print("\n")